// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:instagram/widget/instapost_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Instagram Feedpage',
        home: HomePage());
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 250, 247, 247),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          children: [
            Text(
              'Instagram',
              style: TextStyle(
                  fontSize: 30,
                  fontStyle: FontStyle.normal,
                  fontFamily: 'Billabong',
                  color: Colors.black),
            ),
            Icon(Icons.keyboard_arrow_down, color: Colors.black),
            SizedBox(
              width: 15,
            ),
          ],
        ),
        actions: [
          Icon(Icons.favorite_outline_outlined, color: Colors.black),
          SizedBox(
            width: 15,
          ),
          Image.asset(
            'assets/images/messenger_icon.png',
            // height: 20,
            width: 20,
          ),
          SizedBox(
            width: 15,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Instapost(
              usericon: 'assets/images/ekbana_icon.png',
              username: ' EKbana',
              comment:
                  ' Congratulations to all the interns on being part of the team! 🎉🎉',
              photo: 'assets/images/1.png',
              likenumber: 1000,
              noofcomment: 400,
            ),
            Instapost(
              usericon: 'assets/images/safal.jpg',
              username: ' Safal',
              comment: ' Conquering the mountain 😎',
              photo: 'assets/images/safal.jpg',
              likenumber: 99,
              noofcomment: 12,
            ),
            Instapost(
              usericon: 'assets/images/Tirtha_Shrestha.jpg',
              username: ' Tirtha',
              comment: ' Jimi Pasa pi',
              photo: 'assets/images/group.JPG',
              likenumber: 234,
              noofcomment: 33,
            ),
            Instapost(
              usericon: 'assets/images/wandering.png',
              username: ' Sajish',
              comment: ' Dawn till Dusk',
              photo: 'assets/images/wandering.png',
              likenumber: 12,
              noofcomment: 2,
            ),
            Instapost(
              usericon: 'assets/images/takingpic.png',
              username: ' Salil',
              comment: ' Smile PLZ 😁😁',
              photo: 'assets/images/takingpic.png',
              likenumber: 43,
              noofcomment: 3,
            ),
          ],
        ),
      ),
    );
  }
}
