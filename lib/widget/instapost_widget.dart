import 'package:flutter/material.dart';

class Instapost extends StatelessWidget {
  String? username;
  String? usericon;
  String? photo;
  String? comment;
  int? likenumber;
  int? noofcomment;
  Instapost(
      {required this.username,
      required this.usericon,
      required this.photo,
      required this.comment,
      this.likenumber,
      this.noofcomment});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: CircleAvatar(
                  radius: 16.0,
                  child: ClipRRect(
                    child: Image.asset(
                      usericon!,

                      // 'assets/images/ekbana_icon.png',

                      // width: 40,
                    ),
                    borderRadius: BorderRadius.circular(40.0),
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                username!,
                style: const TextStyle(
                    fontSize: 22,
                    fontStyle: FontStyle.normal,
                    color: Colors.black),
              ),
              const Spacer(),
              const Icon(Icons.more_horiz, color: Colors.black),
              const SizedBox(
                width: 15,
              ),
            ],
          ),
          const SizedBox(height: 8),
          Image.asset(photo!
              // 'assets/images/1.png',
              // height: 400,
              ),
          const SizedBox(height: 8),
          Row(
            children: const [
              SizedBox(
                width: 15,
              ),
              Icon(Icons.favorite_outline, color: Colors.black),
              SizedBox(
                width: 15,
              ),
              Icon(Icons.mode_comment_outlined, color: Colors.black),
              SizedBox(
                width: 15,
              ),
              Icon(Icons.send_outlined, color: Colors.black),
              SizedBox(
                width: 15,
              ),
              Spacer(),
              Icon(Icons.bookmark_border_sharp, color: Colors.black),
              SizedBox(
                width: 15,
              ),
            ],
          ),
          const SizedBox(height: 10),
          Text(
            '  $likenumber  likes',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          RichText(
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
            text: TextSpan(
              style: const TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
              children: <TextSpan>[
                TextSpan(
                    text: username!,
                    style: const TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(
                  text: comment,
                  // text: ' Congratulations on being part of the team! 🎉🎉',
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Text(
            '  View all $noofcomment comments',
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
